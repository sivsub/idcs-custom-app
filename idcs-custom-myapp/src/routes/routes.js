import * as React from "react";
import { Route, Switch } from "react-router-dom";
import { Callback } from "../views/auth/callback";
import { PrivateRoute } from "./privateRoute";
import { PublicPage } from "../views/publicPage";
import DefaultLayout from "../containers/DefaultLayout/DefaultLayout";

export const Routes = (
  <Switch>
    <Route exact={true} path="/signin-oidc" component={Callback} />
    <PrivateRoute path="/" component={DefaultLayout} />
    <Route path="/" component={PublicPage} />
  </Switch>
);
