import React, { Component } from "react";
//import { HashRouter, Route, Switch } from "react-router-dom";
import Config from "./config/default.json";
import AuthProvider from "../src/provider/authProvider";
//import { PrivateRoute } from "./routes/privateRoute";
//import PrivateRoute from "../src/routes/privateRoute";
import { Routes } from "./routes/routes";
import { BrowserRouter } from "react-router-dom";
import "./App.scss";
//import { Callback } from "../src/views/auth/callback";
//import axios from "axios";

const loading = () => <div className="animated fadeIn pt-3 text-center" />;

// Containers
const DefaultLayout = React.lazy(() => import("./containers/DefaultLayout"));

// Pages
const Register = React.lazy(() => import("./views/Pages/Register"));
const Page404 = React.lazy(() => import("./views/Pages/Page404"));
const Page500 = React.lazy(() => import("./views/Pages/Page500"));

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isUserHasAccess: false,
      token: ""
    };

    this.getAccessToken = this.getAccessToken.bind(this);
  }

  async componentDidMount() {
    let token = localStorage.getItem("token");
    if (!token) {
      setTimeout(() => {
        this.getAccessToken();
      }, 3000);
    } else {
      this.getAccessToken();
    }
  }

  async getAccessToken() {
    let curruentComponent = this;
    let tkn = localStorage.getItem("id_token");
    if (!tkn) {
      return "";
    }
    return new Promise(function(resolve, reject) {
      var request = require("request");
      var options = {
        method: "POST",
        url: Config.CUST_SERVICE_ENDPOINT + "/accessToken",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        form: { id_token: localStorage.getItem("id_token") }
      };

      request(options, async function(error, response, body) {
        if (error) {
          window.location.replace(Config.LOGIN_URL);
        }
        if (response.statusCode === 200 && body !== "") {
          curruentComponent.setState({
            isUserHasAccess: true
          });
          localStorage.setItem("token", body);
          resolve(body);
        }
      });
    });
  }
  render() {
    return (
      <AuthProvider>
        <BrowserRouter
          children={Routes}
          basename={"/"}
          token={this.state.token}
        />
      </AuthProvider>
    );
  }
}

export default App;
