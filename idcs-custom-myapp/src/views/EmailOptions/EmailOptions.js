import React, { Component } from "react";
import axios from "axios";
import Config from "../../config/default.json";
import { HashLoader } from "react-spinners";
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  FormGroup,
  Input,
  Label,
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Row
} from "reactstrap";

class EmailOptions extends Component {
  constructor(props) {
    super(props);
    this.togglePrimary = this.togglePrimary.bind(this);
    this.toggleSecondary = this.toggleSecondary.bind(this);
    this.handleVerifyPassword = this.handleVerifyPassword.bind(this);
    this.handleSaveAndVerify = this.handleSaveAndVerify.bind(this);
    this.handlePasswordOnChange = this.handlePasswordOnChange.bind(this);
    this.handelPryEmailChange = this.handelPryEmailChange.bind(this);
    this.handelSecEmailChange = this.handelSecEmailChange.bind(this);
    this.handleSendVerifyEmail = this.handleSendVerifyEmail.bind(this);
    this.toggleVerifyEmailSecondary = this.toggleVerifyEmailSecondary.bind(
      this
    );
    this.toggleVerifyEmailPrimary = this.toggleVerifyEmailPrimary.bind(this);
    this.handleReSendVerifyEmail = this.handleReSendVerifyEmail.bind(this);
  }
  state = {
    signoutTime: 1000 * 60 * 30,
    displayMessage: "",
    displayMessagetype: "",
    primary: false,
    secondary: false,
    verifyEmailSecondary: false,
    verifyEmailPrimary: false,
    disableInputPrimaryEmail: true,
    disableInputSecondaryEmail: true,
    passwordError: "",
    disableSubBtnPasswordPry: true,
    disableSubBtnPasswordSec: true,
    inputPasswordSec: "",
    inputPasswordPry: "",
    targetSaveEmailModel: "",
    showSavePryEmail: false,
    showSaveSecEmail: false,
    primaryEmailError: "",
    secondaryEmailError: "",
    loading: true,

    primaryMail: {
      secondary: "",
      verified: "",
      primary: "",
      value: "",
      type: ""
    },
    secondaryMail: {
      secondary: "",
      verified: "",
      primary: "",
      value: "",
      type: ""
    },
    userName: ""
  };
  togglePrimary() {
    this.setState({
      //      inputPasswordPry: "",
      passwordError: null,
      primary: !this.state.primary
    });
  }
  toggleSecondary() {
    this.setState({
      //      inputPasswordSec: "",
      passwordError: null,
      secondary: !this.state.secondary
    });
  }
  toggleVerifyEmailSecondary() {
    this.setState({
      passwordError: null,
      verifyEmailSecondary: !this.state.verifyEmailSecondary
    });
  }
  toggleVerifyEmailPrimary() {
    this.setState({
      passwordError: null,
      verifyEmailPrimary: !this.state.verifyEmailPrimary
    });
  }

  handleVerifyPassword(e) {
    let currentComponent = this;
    let val = "";
    let emailType = "";
    if (e.currentTarget.name === "btnSubmitSecondary") {
      emailType = "secondary";
      val = currentComponent.state.inputPasswordSec;
    } else if (e.currentTarget.name === "btnSubmitPrimary") {
      emailType = "primary";
      val = currentComponent.state.inputPasswordPry;
    }
    if (val === "" || val === undefined) {
      return;
    }
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    };
    const body = JSON.stringify({
      username: currentComponent.state.userName,
      password: val
    });
    axios
      .post(Config.CUST_SERVICE_ENDPOINT + "/verifyme", body, {
        headers: headers
      })
      .then(function(response) {
        if (response.status === 200) {
          currentComponent.setState({
            primary: false,
            secondary: false
          });
          if (emailType === "secondary") {
            currentComponent.setState({
              disableInputSecondaryEmail: false
            });
            currentComponent.setState({
              showSaveSecEmail: true
            });
          } else if (emailType === "primary") {
            currentComponent.setState({
              disableInputPrimaryEmail: false
            });
            currentComponent.setState({
              showSavePryEmail: true
            });
          }
        } else {
          currentComponent.setState({
            passwordError: " The current password specified is invalid."
          });
        }
      })
      .catch(error => {
        if (error && error.response.status === 401) {
          localStorage.clear();
          window.location.replace(Config.LOGOUT_URL);
        }
      });
  }

  handlePasswordOnChange(e) {
    let val = e.currentTarget.value;
    let intName = e.currentTarget.name;
    if (val !== "" && val.trim() !== "") {
      if (intName === "inputPasswordPry") {
        this.setState({
          inputPasswordPry: val,
          disableSubBtnPasswordPry: false
        });
      }
      if (intName === "inputPasswordSec") {
        this.setState({
          inputPasswordSec: val,
          disableSubBtnPasswordSec: false
        });
      }
    } else {
      this.setState({
        inputPasswordPry: "",
        inputPasswordSec: "",
        disableSubBtnPasswordPry: true,
        disableSubBtnPasswordSec: true
      });
    }
  }

  handleSaveAndVerify(e) {
    if (this.state.primaryMail.value === this.state.secondaryMail.value) {
      this.setState({
        displayMessagetype: "failure",
        displayMessage:
          "You can't specify the same email address for both the primary email address and the secondary email address."
      });
      setTimeout(() => {
        this.setState({
          displayMessagetype: "",
          displayMessage: ""
        });
      }, 5000);
      return false;
    }
    let currentComponent = this;
    let currToken = "";
    if (e.currentTarget.name === "btnSavePrimaryEmail") {
      if (
        currentComponent.state.primaryMail.value === "" ||
        currentComponent.state.primaryMail.value === undefined
      ) {
        currentComponent.setState({
          primaryEmailError: "Please enter a valid email"
        });
        return;
      }
      currToken = currentComponent.state.inputPasswordPry;
      currentComponent.setState({ targetSaveEmailModel: "verifyEmailPrimary" });
    } else if (e.currentTarget.name === "btnSaveSecondaryEmail") {
      if (
        currentComponent.state.secondaryMail.value === undefined ||
        currentComponent.state.secondaryMail.value === ""
      ) {
        currentComponent.setState({
          secondaryEmailError: "Please enter a valid email"
        });
        return;
      }
      currentComponent.setState({
        targetSaveEmailModel: "verifyEmailSecondary"
      });
      currToken = currentComponent.state.inputPasswordSec;
    }

    let emailP = {
      secondary: false,
      primary: true,
      value: this.state.primaryMail.value,
      type: "work"
    };
    let emailS = {
      secondary: true,
      primary: false,
      value: this.state.secondaryMail.value,
      type: "recovery"
    };
    let email = [];
    email.push(emailP);
    if (this.state.secondaryMail.value !== "") {
      email.push(emailS);
    }

    var request = require("request");
    var options = {
      method: "PATCH",
      url: Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/Me",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      },
      body: {
        schemas: ["urn:ietf:params:scim:api:messages:2.0:PatchOp"],
        Operations: [
          {
            op: "replace",
            path:
              "urn:ietf:params:scim:schemas:oracle:idcs:extension:me:User:currentPassword",
            value: currToken
          },
          {
            op: "replace",
            path: "emails",
            value: email
          }
        ]
      },
      json: true
    };

    request(options, function(error, response, body) {
      if (error) {
        if (error && response && response.status === 401) {
          localStorage.clear();
          window.location.replace(Config.LOGOUT_URL);
        }
        currentComponent.setState({
          passwordError: "Error occured, Please try again later."
        });
      }
      if (response.statusCode === 200) {
        let trgModel = currentComponent.state.targetSaveEmailModel;
        currentComponent.setState({ targetSaveEmailModel: "" });
        if (trgModel === "verifyEmailPrimary") {
          currentComponent.setState({ verifyEmailPrimary: true });
        } else if (trgModel === "verifyEmailSecondary") {
          currentComponent.setState({ verifyEmailSecondary: true });
        }
      }
      if (response && response.status === 401) {
        localStorage.clear();
        window.location.replace(Config.LOGOUT_URL);
      }
    });
  }

  handleReSendVerifyEmail(e) {
    if (e.currentTarget.name === "btnReSendVerifyPryEmail") {
      this.setState({ targetSaveEmailModel: "verifyEmailPrimary" });
      this.setState({ verifyEmailPrimary: true });
    } else if (e.currentTarget.name === "btnReSendVerifySecEmail") {
      this.setState({
        targetSaveEmailModel: "verifyEmailSecondary"
      });
      this.setState({ verifyEmailSecondary: true });
    }
  }

  handleSendVerifyEmail(e) {
    let sendBtn = e.currentTarget.name;
    let currentComponent = this;
    let email = "";
    if (sendBtn === "btnSendVerifyPryEmail") {
      email = this.state.primaryMail.value;
    } else if (sendBtn === "btnSendVerifySecEmail") {
      email = this.state.secondaryMail.value;
    }
    var request = require("request");

    var options = {
      method: "PUT",
      url: Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/MeEmailVerifier",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
        "Content-Type": "application/json"
      },
      body: {
        email: email,
        schemas: ["urn:ietf:params:scim:schemas:oracle:idcs:MeEmailVerifier"]
      },
      json: true
    };

    request(options, function(error, response, body) {
      if (error) {
        if (response && response.status === 401) {
          localStorage.clear();
          window.location.replace(Config.LOGOUT_URL);
        }
        currentComponent.setState({
          passwordError: "Error occured, Please try again later."
        });
        currentComponent.setState({
          displayMessagetype: "failure",
          displayMessage: "Error while updateing email, Please try again later"
        });
      }
      if (response.statusCode === 200) {
        currentComponent.setState({
          verifyEmailPrimary: false,
          verifyEmailSecondary: false
        });
        currentComponent.setState({
          displayMessagetype: "success",
          displayMessage:
            "Email sent sucessfully, Please follow the link to verify."
        });
        setTimeout(() => {
          currentComponent.setState({
            displayMessagetype: "",
            displayMessage: ""
          });
        }, 5000);
      }
      if (response && response.status === 401) {
        localStorage.clear();
        window.location.replace(Config.LOGOUT_URL);
      }
    });
  }
  handelPryEmailChange(e) {
    let val = e.currentTarget.value;
    let intName = e.currentTarget.name;
    let email = {};
    if (intName === "inputPrimaryMail") {
      email = this.state.primaryMail;
      email.value = val;
      this.setState({
        primaryMail: email
      });
      if (val.trim() === "") {
        this.setState({
          primaryEmailError: "Please enter a valid email"
        });
      } else {
        this.setState({
          primaryEmailError: ""
        });
      }
    }
  }
  handelSecEmailChange(e) {
    let val = e.currentTarget.value;
    let intName = e.currentTarget.name;
    let email = {};

    if (intName === "inputsecondaryMail") {
      email = this.state.secondaryMail;
      email.value = val;
      this.setState({
        secondaryMail: email
      });
      if (val.trim() === "") {
        this.setState({
          secondaryEmailError: "Please enter a valid email"
        });
      } else {
        this.setState({
          secondaryEmailError: ""
        });
      }
    }
  }
  componentDidMount() {
    this.events = [
      "load",
      "mousemove",
      "mousedown",
      "click",
      "scroll",
      "keypress"
    ];
    for (var i in this.events) {
      window.addEventListener(this.events[i], this.resetTimeout);
    }
    this.setTimeout();

    let currentComponent = this;
    const headers = {
      "Content-Type": "application/json",
      Authorization: "Bearer " + localStorage.getItem("token")
    };
    axios
      .get(Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/Me", { headers: headers })
      .then(function(response) {
        const data = response.data;
        const emails = data.emails;

        const primaryMail = emails.find(function(email) {
          return email.type === "work";
        });
        const secondaryMail = emails.find(function(email) {
          return email.type === "recovery";
        });
        let pryMail = currentComponent.state.primaryMail;
        currentComponent.setState({
          primaryMail: primaryMail !== undefined ? primaryMail : pryMail
        });
        let secMail = currentComponent.state.secondaryMail;
        currentComponent.setState({
          secondaryMail: secondaryMail !== undefined ? secondaryMail : secMail,
          showSaveSecEmail:
            secondaryMail !== undefined && secondaryMail.value !== ""
              ? false
              : true
        });
        currentComponent.setState({ userName: data.userName });
        currentComponent.setState({ loading: false });
      })
      .catch(error => {
        if (error && error.response.status === 401) {
          localStorage.clear();
          window.location.replace(Config.LOGOUT_URL);
        }
      });
  }
  render() {
    if (this.state.loading) {
      return (
        <div>
          <div className="sweet-loading spinner">
            <HashLoader
              sizeUnit={"px"}
              size={50}
              color={"#123abc"}
              loading={this.state.loading}
            />
          </div>
        </div>
      );
    } else {
      return (
        <div className="animated fadeIn">
          <Row>
            <Col className="pull-left">
              <strong>
                <span
                  className={
                    this.state.displayMessagetype === "success"
                      ? "color-success"
                      : "color-error"
                  }
                >
                  {this.state.displayMessage}
                </span>
              </strong>
            </Col>
          </Row>
          <Row>
            <Col xl="10">
              <Card className="mb-0">
                <CardHeader id="headingTwo">
                  <h5 className="m-0 p-0">Primary Email</h5>
                </CardHeader>
                <CardBody>
                  <p>
                    This is the primary email address that email notifications
                    are sent to. To change this email address, click Change.
                  </p>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="inputPrimaryMail">Primary Email</Label>
                    </Col>
                    <Col xs="12" md="4">
                      <Input
                        type="text"
                        id="inputPrimaryMail"
                        name="inputPrimaryMail"
                        placeholder=""
                        onChange={this.handelPryEmailChange}
                        value={this.state.primaryMail.value}
                        disabled={this.state.disableInputPrimaryEmail}
                      />
                      <span className="color-error">
                        {this.state.primaryEmailError}
                      </span>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label>Status</Label>
                    </Col>
                    <Col xs="12" md="4">
                      <Label
                        className={
                          this.state.primaryMail.verified
                            ? "color-success"
                            : "color-error"
                        }
                      >
                        <strong>Email verified</strong>
                      </Label>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col>
                      {this.state.primaryMail.verified === "" ||
                      this.state.primaryMail.verified ||
                      this.state.showSavePryEmail ? (
                        <Button
                          type="submit"
                          size="lg"
                          name="btnSavePrimaryEmail"
                          id="btnSavePrimaryEmail"
                          color="primary"
                          className="btn"
                          disabled={this.state.disableInputPrimaryEmail}
                          onClick={this.handleSaveAndVerify}
                        >
                          <i className="fa fa-dot-circle-o" /> Save & Verify
                        </Button>
                      ) : (
                        <Button
                          type="submit"
                          size="lg"
                          name="btnReSendVerifyPryEmail"
                          color="primary"
                          className="btn"
                          onClick={this.handleReSendVerifyEmail}
                        >
                          <i className="fa fa-dot-circle-o" /> Resend Email
                        </Button>
                      )}
                      &nbsp;&nbsp;
                      <Button
                        type="submit"
                        size="lg"
                        name="btnChangePrimaryEmail"
                        color="primary"
                        className="btn"
                        onClick={this.togglePrimary}
                      >
                        <i className="fa fa-edit" /> Change
                      </Button>
                    </Col>
                    <Col xs="12" md="2">
                      <Modal
                        isOpen={this.state.primary}
                        toggle={this.togglePrimary}
                        className={"modal-primary " + this.props.className}
                      >
                        <ModalHeader toggle={this.togglePrimary}>
                          Credentials
                        </ModalHeader>
                        <ModalBody>
                          In order to change your email you need to enter your
                          password.
                          <br />
                          <Row className="divpro">
                            <Col md="4">
                              <Label htmlFor="inputPasswordPry">
                                * Password
                              </Label>
                            </Col>
                            <Col xs="12" md="6">
                              <Input
                                type="password"
                                id="inputPasswordPry"
                                name="inputPasswordPry"
                                placeholder="Password"
                                value={this.state.inputPasswordPry}
                                onChange={this.handlePasswordOnChange}
                              />
                            </Col>
                          </Row>
                          <Row className="divpro">
                            <Col xs="12" md="8">
                              <Label className="color-error">
                                <strong>{this.state.passwordError}</strong>
                              </Label>
                            </Col>
                          </Row>
                        </ModalBody>
                        <ModalFooter>
                          <Button
                            color="primary"
                            onClick={this.handleVerifyPassword}
                            disabled={this.state.disableSubBtnPasswordPry}
                            name="btnSubmitPrimary"
                            id="btnSubmitPrimary"
                          >
                            Submit
                          </Button>{" "}
                          <Button
                            color="secondary"
                            onClick={this.togglePrimary}
                          >
                            Cancel
                          </Button>
                        </ModalFooter>
                      </Modal>
                      <Modal
                        isOpen={this.state.verifyEmailPrimary}
                        toggle={this.toggleVerifyEmailPrimary}
                        className={"modal-primary " + this.props.className}
                      >
                        <ModalHeader toggle={this.toggleVerifyEmailPrimary}>
                          Verify your recovery email address
                        </ModalHeader>
                        <ModalBody>
                          Verify your recovery email address, and then click
                          Send to receive a new verification email.
                          <br />
                          <Row className="divpro">
                            <Col md="4">
                              <Label>
                                <i className="fa fa-envelope" />{" "}
                                {this.state.primaryMail.value}
                              </Label>
                            </Col>
                          </Row>
                          <Row className="divpro">
                            <Col xs="12" md="8">
                              <Label className="color-error">
                                <strong>{this.state.passwordError}</strong>
                              </Label>
                            </Col>
                          </Row>
                        </ModalBody>
                        <ModalFooter>
                          <Button
                            color="primary"
                            onClick={this.handleSendVerifyEmail}
                            name="btnSendVerifyPryEmail"
                            id="btnSendVerifyPryEmail"
                          >
                            Send
                          </Button>
                          <Button
                            color="secondary"
                            onClick={this.toggleVerifyEmailPrimary}
                          >
                            Close
                          </Button>
                        </ModalFooter>
                      </Modal>
                    </Col>
                  </FormGroup>
                </CardBody>
              </Card>
              <br />
              <Card className="mb-0">
                <CardHeader id="headingOne">
                  <h5 className="m-0 p-0">Recovery Emaill</h5>
                </CardHeader>
                <CardBody>
                  <p>To change your recovery email address, click Change.</p>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="inputsecondaryMail">Recovery Email</Label>
                    </Col>
                    <Col xs="12" md="4">
                      <Input
                        type="text"
                        id="inputsecondaryMail"
                        name="inputsecondaryMail"
                        placeholder=""
                        onChange={this.handelSecEmailChange}
                        value={this.state.secondaryMail.value}
                        disabled={this.state.disableInputSecondaryEmail}
                      />
                      <span className="color-error">
                        {this.state.secondaryEmailError}
                      </span>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label>Status</Label>
                    </Col>
                    <Col xs="12" md="4">
                      <Label
                        className={
                          this.state.secondaryMail.verified
                            ? "color-success"
                            : "color-error"
                        }
                      >
                        <strong>Email verified</strong>
                      </Label>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col>
                      {this.state.secondaryMail.verified === "" ||
                      this.state.secondaryMail.verified ||
                      this.state.showSaveSecEmail ? (
                        <Button
                          type="submit"
                          size="lg"
                          name="btnSaveSecondaryEmail"
                          id="btnSaveSecondaryEmail"
                          color="primary"
                          className="btn"
                          disabled={this.state.disableInputSecondaryEmail}
                          onClick={this.handleSaveAndVerify}
                        >
                          <i className="fa fa-dot-circle-o" /> Save & Verify
                        </Button>
                      ) : (
                        <Button
                          type="submit"
                          size="lg"
                          name="btnReSendVerifySecEmail"
                          color="primary"
                          className="btn"
                          onClick={this.handleReSendVerifyEmail}
                        >
                          <i className="fa fa-dot-circle-o" /> Resend Email
                        </Button>
                      )}
                      &nbsp;&nbsp;
                      <Button
                        type="submit"
                        size="lg"
                        name="btnChangeSecondaryEmail"
                        color="primary"
                        className="btn"
                        onClick={this.toggleSecondary}
                      >
                        <i className="fa fa-edit" /> Change
                      </Button>
                    </Col>
                    <Col xs="12" md="2">
                      <Modal
                        isOpen={this.state.secondary}
                        toggle={this.toggleSecondary}
                        className={"modal-primary " + this.props.className}
                      >
                        <ModalHeader toggle={this.toggleSecondary}>
                          Credentials
                        </ModalHeader>
                        <ModalBody>
                          In order to change your email you need to enter your
                          password.
                          <br />
                          <Row className="divpro">
                            <Col md="4">
                              <Label htmlFor="inputPasswordSec">
                                * Password
                              </Label>
                            </Col>
                            <Col xs="12" md="6">
                              <Input
                                type="password"
                                id="inputPasswordSec"
                                name="inputPasswordSec"
                                placeholder="Password"
                                onChange={this.handlePasswordOnChange}
                                value={this.state.inputPasswordSec}
                              />
                            </Col>
                          </Row>
                          <Row className="divpro">
                            <Col xs="12" md="8">
                              <Label className="color-error">
                                <strong>{this.state.passwordError}</strong>
                              </Label>
                            </Col>
                          </Row>
                        </ModalBody>
                        <ModalFooter>
                          <Button
                            color="primary"
                            onClick={this.handleVerifyPassword}
                            disabled={this.state.disableSubBtnPasswordSec}
                            name="btnSubmitSecondary"
                            id="btnSubmitSecondary"
                          >
                            Submit
                          </Button>{" "}
                          <Button
                            color="secondary"
                            onClick={this.toggleSecondary}
                          >
                            Cancel
                          </Button>
                        </ModalFooter>
                      </Modal>
                      <Modal
                        isOpen={this.state.verifyEmailSecondary}
                        toggle={this.toggleVerifyEmailSecondary}
                        className={"modal-primary " + this.props.className}
                      >
                        <ModalHeader toggle={this.toggleVerifyEmailSecondary}>
                          Verify your recovery email address
                        </ModalHeader>
                        <ModalBody>
                          Verify your recovery email address, and then click
                          Send to receive a new verification email.
                          <br />
                          <Row className="divpro">
                            <Col md="4">
                              <Label htmlFor="inputPasswordSec">
                                <i className="fa fa-envelope" />{" "}
                                {this.state.secondaryMail.value}
                              </Label>
                            </Col>
                          </Row>
                          <Row className="divpro">
                            <Col xs="12" md="8">
                              <Label className="color-error">
                                <strong>{this.state.passwordError}</strong>
                              </Label>
                            </Col>
                          </Row>
                        </ModalBody>
                        <ModalFooter>
                          <Button
                            color="primary"
                            onClick={this.handleSendVerifyEmail}
                            name="btnSendVerifySecEmail"
                            id="btnSendVerifySecEmail"
                          >
                            Send
                          </Button>
                          <Button
                            color="secondary"
                            onClick={this.toggleVerifyEmailSecondary}
                          >
                            Close
                          </Button>
                        </ModalFooter>
                      </Modal>
                    </Col>
                  </FormGroup>
                </CardBody>
              </Card>
              <br />
            </Col>
          </Row>
        </div>
      );
    }
  }
  clearTimeoutFunc = () => {
    if (this.logoutTimeout) clearTimeout(this.logoutTimeout);
  };

  setTimeout = () => {
    this.logoutTimeout = setTimeout(this.logout, this.state.signoutTime);
  };

  resetTimeout = () => {
    this.clearTimeoutFunc();
    this.setTimeout();
  };

  logout = () => {
    console.log("Sending a logout request...");
    this.destroy();
  };

  destroy = () => {
    localStorage.clear();
    window.location.replace(Config.LOGOUT_URL);
  };
}

export default EmailOptions;
