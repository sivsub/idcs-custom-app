import React, { Component } from "react";
import Config from "../../config/default.json";
import { HashLoader } from "react-spinners";
import { Row } from "reactstrap";
class Dashboard extends Component {
  constructor(props) {
    super(props);
    this.getDashboard = this.getDashboard.bind(this);
  }
  state = {
    loading: true,
    errorMessage: "",
    mydiv: [],
    signoutTime: 1000 * 60 * 30
  };

  async getDashboard() {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");
      var options = {
        method: "GET",
        url:
          Config.IDCS_SERVICE_ENDPOINT +
          "/admin/v1/MyApps?count=48&startIndex=1&sortBy=app.display&sortOrder=ascending&filter=active%20eq%20true&attributes=id%2Cactive%2Cfavorite%2Claunc" +
          "hUrl%2ClastAccessed%2CuserWalletArtifact%2Capp.appIcon%2Capp.loginMechanism%2Capp.display%2Capp.value",

        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("token")
        }
      };

      request(options, async function(error, response, body) {
        if (error) {
          currentComponent.setState({
            errorMessage:
              "Error while loading your dashboard, Please try again later",
            loading: false
          });
        }
        if (response.statusCode === 200) {
          let data = JSON.parse(body);
          let appsList = data.Resources;

          let ap = appsList.map(myap => {
            return (
              <div key={myap.id} className="app-tile">
                <a
                  href={Config.IDCS_SERVICE_ENDPOINT + myap.launchUrl}
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <div className="my-apps">
                    <img src={myap.app.appIcon} alt="" />
                    <span className="caption">{myap.app.display}</span>
                  </div>
                </a>
              </div>
            );
          });
          currentComponent.setState({
            errorMessage: "",
            mydiv: ap,
            loading: false
          });
          resolve(data);
        }
      });
    });
  }
  async componentDidMount() {
    let token = localStorage.getItem("token");
    if (!token) {
      setTimeout(() => {
        this.getDashboard();
      }, 4000);
    } else {
      this.getDashboard();
    }

    this.events = [
      "load",
      "mousemove",
      "mousedown",
      "click",
      "scroll",
      "keypress"
    ];

    for (var i in this.events) {
      window.addEventListener(this.events[i], this.resetTimeout);
    }

    this.setTimeout();
  }

  render() {
    if (this.state.loading) {
      return (
        <div>
          <div className="sweet-loading spinner">
            <HashLoader
              sizeUnit={"px"}
              size={50}
              color={"#123abc"}
              loading={this.state.loading}
            />
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <Row>{this.state.errorMessage}</Row>
          <Row>{this.state.mydiv}</Row>
        </div>
      );
    }
  }

  clearTimeoutFunc = () => {
    if (this.logoutTimeout) clearTimeout(this.logoutTimeout);
  };

  setTimeout = () => {
    this.logoutTimeout = setTimeout(this.logout, this.state.signoutTime);
  };

  resetTimeout = () => {
    this.clearTimeoutFunc();
    this.setTimeout();
  };

  logout = () => {
    console.log("Sending a logout request...");
    this.destroy();
  };

  destroy = () => {
    localStorage.clear();
    window.location.replace(Config.LOGOUT_URL);
  };
}

export default Dashboard;
