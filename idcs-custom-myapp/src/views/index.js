import Dashboard from "./Dashboard";
import ChangePassword from "./ChangePassword";
import EmailOptions from "./EmailOptions";
import MyProfile from "./MyProfile";
import MultiFactor from "./MultiFactor";
import Logout from "./Logout";

export {
  Dashboard,
  ChangePassword,
  EmailOptions,
  MyProfile,
  MultiFactor,
  Logout
};
