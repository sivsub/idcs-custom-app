import React, { Component } from "react";
import { AppSwitch } from "@coreui/react";
import Config from "../../config/default.json";
import { HashLoader } from "react-spinners";
import {
  Col,
  Button,
  Row,
  Input,
  Table,
  InputGroup,
  InputGroupAddon,
  Modal,
  ModalFooter,
  ModalBody,
  FormGroup,
  ModalHeader,
  Card,
  CardHeader,
  CardBody
} from "reactstrap";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemPanel,
  AccordionItemButton
} from "react-accessible-accordion";
import "react-accessible-accordion/dist/fancy-example.css";
class MultiFactor extends Component {
  constructor(props) {
    super(props);

    this.getUserProfile = this.getUserProfile.bind(this);
    this.handleBtnEmailSetupClick = this.handleBtnEmailSetupClick.bind(this);
    this.handleBtnAddMobileNumberClick = this.handleBtnAddMobileNumberClick.bind(
      this
    );
    this.displayStepVerificationContent = this.displayStepVerificationContent.bind(
      this
    );
    this.stepVerificationSwitch = this.stepVerificationSwitch.bind(this);
    this.toggleEmailSetupModal = this.toggleEmailSetupModal.bind(this);
    this.toggleMobileNumberSetupModal = this.toggleMobileNumberSetupModal.bind(
      this
    );
    this.toggleMobileAppSetupModal = this.toggleMobileAppSetupModal.bind(this);
    this.postEmailAuthenticationFactorEnroller = this.postEmailAuthenticationFactorEnroller.bind(
      this
    );
    this.postEmailAuthenticationFactorInitiator = this.postEmailAuthenticationFactorInitiator.bind(
      this
    );
    this.handleBtnVerifyEmailPasscodeClick = this.handleBtnVerifyEmailPasscodeClick.bind(
      this
    );
    this.handleOnChangeInputEmailPasscode = this.handleOnChangeInputEmailPasscode.bind(
      this
    );
    this.handleBtnAddQRCodeSetupClick = this.handleBtnAddQRCodeSetupClick.bind(
      this
    );
    this.handleAddMobileNumberModalChange = this.handleAddMobileNumberModalChange.bind(
      this
    );
    this.handleBtnAddMobileNumberSendOTPClick = this.handleBtnAddMobileNumberSendOTPClick.bind(
      this
    );
    this.saveOTPMobileNumberAndInitiate = this.saveOTPMobileNumberAndInitiate.bind(
      this
    );
    this.requestOTPbySMS = this.requestOTPbySMS.bind(this);
    this.handleBtnVerifyMobileSMSOtpClick = this.handleBtnVerifyMobileSMSOtpClick.bind(
      this
    );
    this.getStepVerificationDiviceList = this.getStepVerificationDiviceList.bind(
      this
    );
    this.btnRemoveOTPDevice = this.btnRemoveOTPDevice.bind(this);
    this.deleteSingleMFADevice = this.deleteSingleMFADevice.bind(this);
    this.toggleRemoveOTPDeviceModal = this.toggleRemoveOTPDeviceModal.bind(
      this
    );
    this.handleBtnRemoveOTPConfirm = this.handleBtnRemoveOTPConfirm.bind(this);
    this.getQRCodeImage = this.getQRCodeImage.bind(this);
    this.disableUserAllMFADevices = this.disableUserAllMFADevices.bind(this);
    this.toggleRemoveAllOTPDeviceModal = this.toggleRemoveAllOTPDeviceModal.bind(
      this
    );
    this.handleBtnRemoveAllOTP = this.handleBtnRemoveAllOTP.bind(this);
    this.changeDefaultMFAType = this.changeDefaultMFAType.bind(this);
    this.toggleChangeDefaultMFAModal = this.toggleChangeDefaultMFAModal.bind(
      this
    );
    this.handleInputDefaultMFASelection = this.handleInputDefaultMFASelection.bind(
      this
    );
    this.handleBtnChangeDefaultMFAConfirm = this.handleBtnChangeDefaultMFAConfirm.bind(
      this
    );
  }
  state = {
    showErrorMessage: false,
    errorMessage: "",
    signoutTime: 1000 * 60 * 30,
    activeTab: new Array(3).fill("1"),
    mobileNumberSetupModalDisplay: false,
    displayStepVerification: false,
    displayRemoveAllOtpModal: false,
    displayChangeDefaultMFAModal: false,
    stepVerificationStatus: "Disabled",
    emailSetupModalDisplay: false,
    mobileAppSetupModalDisplay: false,
    inputEmailPasscode: "",
    emailSetupDeviceId: "",
    emailSetupRequestId: "",
    emailSetupProcessCompleted: false,
    emailSetupValidationMessage: "",
    inputAddMobileModalName: "",
    inputAddMobileModalNumber: "",
    mobileNumberSetupDeviceId: "",
    mobileNumberSetupRequestId: "",
    mobileNumberSMSSetupMessage: "",
    inputMobileSMSOtp: "",
    displayMobileNumberOTPVerify: false,
    hideEnroleMobileSMSOTPMessage: false,
    hideEnroleMobileAppMessage: false,
    disableinputAddMobileModalField: false,
    mfaDataLoaded: false,
    userHasMFASetup: false,
    loading: true,
    displayRemoveOTPDeviceModal: false,
    listEnrolledMobileNumbers: [],
    listEnrolledQRDevices: [],
    otpRemoveHeader: "",
    otpRemoveBody: "",
    otpRemoveId: "",
    primaryMail: {
      secondary: "",
      verified: "",
      primary: "",
      value: "",
      type: ""
    },
    userName: "",
    title: "",
    userId: "",
    preferredMFADeviceId: "",
    temporaryPreferredMFADeviceId: "",
    disableBtnConfirmPreferredMFA: true,
    preferredAuthFactor: {
      preferredAuthenticationFactor: "",
      mfaStatus: "",
      value: ""
    },
    otpEnrolledEmail: {
      lastSyncTime: "",
      id: "",
      phoneNumber: "",
      status: "",
      displayName: "",
      authenticationFactors: []
    },
    otpMobileQRCode: {
      id: "",
      displayName: "",
      qrCodeImgContent: "",
      qrCodeContent: "",
      qrCodeImgType: "",
      authnFactors: []
    }
  };

  handleBtnChangeDefaultMFAConfirm() {
    let currentComponent = this;

    var request = require("request");
    let type = "";
    if (type === "") {
      if (
        currentComponent.state.otpEnrolledEmail.id ===
        this.state.preferredMFADeviceId
      ) {
        type = "EMAIL";
      }
    }
    if (type === "") {
      let mobile = currentComponent.state.listEnrolledMobileNumbers.filter(
        mobile => mobile.id === this.state.preferredMFADeviceId
      );
      if (mobile) {
        type = "SMS";
      }
    }
    if (type === "") {
      let device = currentComponent.state.listEnrolledQRDevices.filter(
        device => device.id === this.state.preferredMFADeviceId
      );
      if (device) {
        type = "PUSH";
      }
    }

    var options = {
      method: "PATCH",
      url: Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/Me",
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + localStorage.getItem("token")
      },
      body: {
        schemas: ["urn:ietf:params:scim:api:messages:2.0:PatchOp"],
        Operations: [
          {
            op: "replace",
            path:
              "urn:ietf:params:scim:schemas:oracle:idcs:extension:mfa:User:preferredAuthenticationFactor",
            value: type
          },
          {
            op: "replace",
            path:
              "urn:ietf:params:scim:schemas:oracle:idcs:extension:mfa:User:preferredDevice",
            value: { value: currentComponent.state.preferredMFADeviceId }
          }
        ]
      },
      json: true
    };

    request(options, function(error, response, body) {
      if (error) {
        if (error && error.response.status === 401) {
          localStorage.clear();
          window.location.replace(Config.LOGOUT_URL);
        }
        currentComponent.setState({
          preferredMFADeviceId:
            currentComponent.state.temporaryPreferredMFADeviceId,
          displayChangeDefaultMFAModal: false
        });
      }

      if (response && response.statusCode === 200) {
        currentComponent.setState({
          temporaryPreferredMFADeviceId:
            currentComponent.state.preferredMFADeviceId,
          displayChangeDefaultMFAModal: false
        });
      }
    });
  }
  handleInputDefaultMFASelection(e) {
    this.setState({
      preferredMFADeviceId: e.currentTarget.id,
      disableBtnConfirmPreferredMFA: false
    });
  }
  changeDefaultMFAType() {
    this.setState({
      displayChangeDefaultMFAModal: true
    });
  }
  handleBtnRemoveAllOTP() {
    this.disableUserAllMFADevices(this.state.userId);
  }
  disableUserAllMFADevices(userId) {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");
      var options = {
        method: "POST",
        url:
          Config.IDCS_SERVICE_ENDPOINT +
          "/admin/v1/MyAuthenticationFactorsRemover",
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
          "Content-Type": "application/json"
        },
        body: {
          user: { value: userId },
          schemas: [
            "urn:ietf:params:scim:schemas:oracle:idcs:AuthenticationFactorsRemover"
          ]
        },
        json: true
      };
      request(options, function(error, response, body) {
        if (error) {
          if (error && error.response.status === 401) {
            localStorage.clear();
            window.location.replace(Config.LOGOUT_URL);
          }
          currentComponent.setState({
            displayRemoveAllOtpModal: false
          });
          reject(error);
        }
        if (response && response.statusCode === 201) {
          currentComponent.setState({
            displayRemoveAllOtpModal: false,
            displayStepVerification: false,
            userHasMFASetup: false
          });
          currentComponent.getStepVerificationDiviceList();
          resolve(body);
        }
      });
    });
  }

  btnRemoveOTPDevice(e) {
    let type = e.currentTarget.name;
    let id = e.currentTarget.id;
    let headerMsg = "";
    let bodyMsg = "";
    if (type === "btnRemoveOTPEmail") {
      headerMsg = "Remove Email?";
      bodyMsg =
        "Are you sure that you want to remove email as a 2-Step Verification method?";
    } else if (type === "SMS") {
      headerMsg = "Remove mobile number?";
      bodyMsg = "Are you sure that you want to remove the mobile number?";
    } else if (type === "PUSH") {
      headerMsg = "Remove mobile app?";
      bodyMsg = "Are you sure that you want to remove the mobile app?";
    }
    this.setState({
      otpRemoveHeader: headerMsg,
      otpRemoveBody: bodyMsg,
      otpRemoveId: id,
      displayRemoveOTPDeviceModal: true
    });
  }

  async handleBtnRemoveOTPConfirm() {
    await this.deleteSingleMFADevice();
    setTimeout(() => {
      this.setState({
        errorMessage: "",
        showErrorMessage: false
      });
    }, 5000);
  }
  async deleteSingleMFADevice() {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");

      var options = {
        method: "DELETE",
        url:
          Config.IDCS_SERVICE_ENDPOINT +
          "/admin/v1/MyDevices/" +
          currentComponent.state.otpRemoveId,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + localStorage.getItem("token")
        }
      };

      request(options, async function(error, response, body) {
        if (error) {
          if (error && error.response.status === 401) {
            localStorage.clear();
            window.location.replace(Config.LOGOUT_URL);
          }
          currentComponent.setState({
            mobileNumberSMSSetupMessage:
              "Error while processing your request. Please try again later"
          });
          reject(error);
        }
        if (response && response.statusCode !== 400) {
          currentComponent.getStepVerificationDiviceList();
          resolve(body);
        }
        if (response && response.statusCode === 400) {
          var bodyData = JSON.parse(body);
          if (bodyData) {
            currentComponent.setState({
              errorMessage: bodyData.detail,
              showErrorMessage: true,
              displayRemoveOTPDeviceModal: false
            });
          }

          resolve(body);
        }
      });
    });
  }

  handleOnChangeInputEmailPasscode(e) {
    this.setState({
      inputEmailPasscode: e.currentTarget.value
    });
  }
  async handleBtnVerifyMobileSMSOtpClick() {
    if (this.state.inputMobileSMSOtp === "") {
      this.setState({
        mobileNumberSMSSetupMessage: "Please Enter SMS OTP"
      });
      return;
    }
    let data = await this.validateMobileNumberSMSOtp();
    if (data !== undefined && data.status === "401") {
      this.setState({
        mobileNumberSMSSetupMessage: "Please Enter SMS OTP"
      });
    } else if (data !== undefined && data.status === "SUCCESS") {
      await this.getStepVerificationDiviceList();
      this.setState({
        mobileNumberSMSSetupMessage: "",
        mobileNumberSetupModalDisplay: false,
        hideEnroleMobileSMSOTPMessage: true
      });
    }
  }
  async validateMobileNumberSMSOtp() {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");

      var options = {
        method: "POST",
        url:
          Config.IDCS_SERVICE_ENDPOINT +
          "/admin/v1/MyAuthenticationFactorValidator",
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
          "Content-Type": "application/json"
        },
        body: {
          schemas: [
            "urn:ietf:params:scim:schemas:oracle:idcs:AuthenticationFactorValidator"
          ],
          deviceId: currentComponent.state.mobileNumberSetupDeviceId,
          requestId: currentComponent.state.mobileNumberSetupRequestId,
          otpCode: currentComponent.state.inputMobileSMSOtp,
          authFactor: "SMS",
          scenario: "ENROLLMENT"
        },
        json: true
      };

      request(options, async function(error, response, body) {
        if (error) {
          if (error && error.response.status === 401) {
            localStorage.clear();
            window.location.replace(Config.LOGOUT_URL);
          }
          currentComponent.setState({
            mobileNumberSMSSetupMessage:
              "Error while processing your request. Please try again later"
          });
          reject(error);
        }
        if (response.statusCode === 201) {
          resolve(body);
        }
      });
    });
  }
  handleAddMobileNumberModalChange(e) {
    if (e.currentTarget.name === "inputAddMobileModalName") {
      this.setState({
        inputAddMobileModalName: e.currentTarget.value
      });
    }
    if (e.currentTarget.name === "inputAddMobileModalNumber") {
      this.setState({
        inputAddMobileModalNumber: e.currentTarget.value
      });
    }
    if (e.currentTarget.name === "inputMobileSMSOtp") {
      this.setState({
        inputMobileSMSOtp: e.currentTarget.value
      });
    }
  }
  async handleBtnAddMobileNumberSendOTPClick() {
    if (
      this.state.inputAddMobileModalName.trim === "" ||
      this.state.inputAddMobileModalNumber === "" ||
      this.state.inputAddMobileModalNumber.length !== 10
    ) {
      this.setState({
        mobileNumberSMSSetupMessage:
          "Please enter a valid Name and Mobile number."
      });
      return;
    }
    let data = await this.saveOTPMobileNumberAndInitiate();

    if (data !== undefined && data.status === "400") {
      this.setState({
        mobileNumberSMSSetupMessage:
          "The phone number is already enrolled. Please add different number"
      });
      return;
    }
    let sms = await this.requestOTPbySMS(data);
    if (sms.requestId !== undefined) {
      this.setState({
        displayMobileNumberOTPVerify: true
      });
    }
  }

  async requestOTPbySMS(data) {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");

      var options = {
        method: "POST",
        url:
          Config.IDCS_SERVICE_ENDPOINT +
          "/admin/v1/MyAuthenticationFactorInitiator",
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
          "Content-Type": "application/json"
        },
        body: {
          schemas: [
            "urn:ietf:params:scim:schemas:oracle:idcs:AuthenticationFactorInitiator"
          ],
          deviceId: currentComponent.state.mobileNumberSetupDeviceId,
          requestId: currentComponent.state.mobileNumberSetupRequestId,
          userName: currentComponent.state.userName,
          authFactor: "SMS"
        },
        json: true
      };

      request(options, async function(error, response, body) {
        if (error) {
          if (error && error.response.status === 401) {
            localStorage.clear();
            window.location.replace(Config.LOGOUT_URL);
          }
          currentComponent.setState({
            mobileNumberSMSSetupMessage:
              "Error while processing your request. Please try again later"
          });
          reject(error);
        }
        if (response.statusCode === 201) {
          resolve(body);
        }
      });
    });
  }

  async saveOTPMobileNumberAndInitiate() {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");

      var options = {
        method: "POST",
        url:
          Config.IDCS_SERVICE_ENDPOINT +
          "/admin/v1/MyAuthenticationFactorEnroller",
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
          "Content-Type": "application/json"
        },
        body: {
          schemas: [
            "urn:ietf:params:scim:schemas:oracle:idcs:AuthenticationFactorEnroller"
          ],
          user: { value: currentComponent.state.userId },
          displayName: currentComponent.state.inputAddMobileModalName,
          phoneNumber: "1" + currentComponent.state.inputAddMobileModalNumber,
          authnFactors: ["SMS"]
        },
        json: true
      };

      request(options, async function(error, response, body) {
        if (error) {
          if (error.response.status === 401) {
            localStorage.clear();
            window.location.replace(Config.LOGIN_URL);
          } else {
            currentComponent.setState({
              mobileNumberSMSSetupMessage:
                "Error while processing your request. Please try again later"
            });
            reject(error);
          }
        }
        if (response.statusCode === 201) {
          currentComponent.setState({
            mobileNumberSetupDeviceId: body.deviceId,
            mobileNumberSetupRequestId: body.requestId
          });
        }
        if (body.status === "400") {
          currentComponent.setState({
            mobileNumberSMSSetupMessage:
              "The phone number is already enrolled. Please add different number"
          });
        }
        resolve(body);
      });
    });
  }

  handleBtnVerifyEmailPasscodeClick() {
    if (
      this.state.inputEmailPasscode === "" ||
      this.state.inputEmailPasscode.length < 6
    ) {
      this.setState({
        emailSetupValidationMessage: "Please enter a valid Passcode."
      });
      return;
    }
    let currentComponent = this;

    var request = require("request");

    var options = {
      method: "POST",
      url:
        Config.IDCS_SERVICE_ENDPOINT +
        "/admin/v1/MyAuthenticationFactorValidator",
      headers: {
        Authorization: "Bearer " + localStorage.getItem("token"),
        "Content-Type": "application/json"
      },
      body: {
        schemas: [
          "urn:ietf:params:scim:schemas:oracle:idcs:AuthenticationFactorValidator"
        ],
        deviceId: currentComponent.state.emailSetupDeviceId,
        requestId: currentComponent.state.emailSetupRequestId,
        otpCode: currentComponent.state.inputEmailPasscode,
        authFactor: "EMAIL",
        scenario: "ENROLLMENT"
      },
      json: true
    };
    request(options, async function(error, response, body) {
      if (error) {
        if (error.response.status === 401) {
          localStorage.clear();
          window.location.replace(Config.LOGIN_URL);
        } else {
          currentComponent.setState({
            emailSetupValidationMessage: "Please enter valid Passcode."
          });
        }
      }
      if (response.statusCode === 201) {
        await currentComponent.getStepVerificationDiviceList();
        currentComponent.setState({
          emailSetupProcessCompleted: true
        });
      } else {
        currentComponent.setState({
          //emailSetupModalDisplay: false,
          emailSetupValidationMessage: "Please enter valid Passcode."
        });
      }
    });
  }
  async handleBtnEmailSetupClick() {
    this.setState({
      emailSetupModalDisplay: true
    });
    let enrollerData = await this.postEmailAuthenticationFactorEnroller();
    await this.postEmailAuthenticationFactorInitiator(enrollerData);
  }

  async postEmailAuthenticationFactorInitiator(data) {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");

      var options = {
        method: "POST",
        url:
          Config.IDCS_SERVICE_ENDPOINT +
          "/admin/v1/MyAuthenticationFactorInitiator",
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
          "Content-Type": "application/json"
        },
        body: {
          schemas: [
            "urn:ietf:params:scim:schemas:oracle:idcs:AuthenticationFactorInitiator"
          ],
          deviceId: data.deviceId,
          requestId: data.requestId,
          userName: currentComponent.state.userName,
          authFactor: "EMAIL"
        },
        json: true
      };

      request(options, async function(error, response, body) {
        if (error) {
          if (error.response.status === 401) {
            localStorage.clear();
            window.location.replace(Config.LOGIN_URL);
          } else {
            reject(error);
          }
        }
        if (response.statusCode === 201) {
          resolve(body);
        }
      });
    });
  }

  async postEmailAuthenticationFactorEnroller() {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");

      var options = {
        method: "POST",
        url:
          Config.IDCS_SERVICE_ENDPOINT +
          "/admin/v1/MyAuthenticationFactorEnroller",
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
          "Content-Type": "application/json"
        },
        body: {
          schemas: [
            "urn:ietf:params:scim:schemas:oracle:idcs:AuthenticationFactorEnroller"
          ],
          user: { value: currentComponent.state.userId },
          authnFactors: ["EMAIL"]
        },
        json: true
      };

      request(options, async function(error, response, body) {
        if (error) {
          if (error.response.status === 401) {
            localStorage.clear();
            window.location.replace(Config.LOGIN_URL);
          } else {
            reject(error);
          }
        }

        if (response.statusCode === 201 && body !== undefined) {
          currentComponent.setState({
            emailSetupDeviceId: body.deviceId,
            emailSetupRequestId: body.requestId
          });

          resolve(body);
        }
      });
    });
  }

  stepVerificationSwitch(e) {
    this.setState({
      displayRemoveAllOtpModal: true
    });
  }

  async getStepVerificationDiviceList() {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");

      var options = {
        method: "GET",
        url: Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/MyDevices",
        headers: {
          "accept-encoding": "gzip, deflate",
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("token")
        }
      };

      request(options, async function(error, response, body) {
        if (error) {
          if (error.response.status === 401) {
            localStorage.clear();
            window.location.replace(Config.LOGIN_URL);
          } else {
            reject(error);
          }
        }
        if (response.statusCode === 200 && body !== undefined) {
          currentComponent.setState({
            loading: false,
            listEnrolledQRDevices: [],
            listEnrolledMobileNumbers: []
          });
          let data = JSON.parse(body);
          let list = data.Resources;
          let mobileList = currentComponent.state.listEnrolledMobileNumbers;
          let QRdiviceList = currentComponent.state.listEnrolledQRDevices;
          list.map(function(item) {
            let divice = {
              lastSyncTime: item.lastSyncTime,
              id: item.id,
              phoneNumber: item.phoneNumber,
              status: item.status,
              displayName: item.displayName,
              authenticationFactors: item.authenticationFactors
            };
            if (
              item.authenticationFactors[0] !== undefined &&
              item.authenticationFactors[0].type === "EMAIL" &&
              item.authenticationFactors[0].status === "ENROLLED"
            ) {
              currentComponent.setState({
                userHasMFASetup: true,
                emailSetupProcessCompleted: true,
                otpEnrolledEmail: divice
              });
            } else if (
              item.authenticationFactors[0] !== undefined &&
              item.authenticationFactors[0].type === "SMS" &&
              item.authenticationFactors[0].status === "ENROLLED"
            ) {
              mobileList.push(divice);
            } else if (
              item.authenticationFactors[0] !== undefined &&
              item.authenticationFactors[0].status === "ENROLLED" &&
              (item.authenticationFactors[0].type === "PUSH" ||
                item.authenticationFactors[0].type === "TOTP")
            ) {
              QRdiviceList.push(divice);
            }
          });

          currentComponent.setState({
            loading: false,
            listEnrolledQRDevices: QRdiviceList,
            listEnrolledMobileNumbers: mobileList
          });
          if (mobileList.length > 0) {
            currentComponent.setState({
              userHasMFASetup: true,
              hideEnroleMobileSMSOTPMessage: true
            });
          } else {
            currentComponent.setState({
              hideEnroleMobileSMSOTPMessage: false
            });
          }
          if (QRdiviceList.length > 0) {
            currentComponent.setState({
              userHasMFASetup: true,
              hideEnroleMobileAppMessage: true
            });
          } else {
            currentComponent.setState({
              hideEnroleMobileAppMessage: false
            });
          }
          currentComponent.setState({
            displayRemoveOTPDeviceModal: false
          });
          resolve(response);
        }
      });
    });
  }
  async displayStepVerificationContent() {
    this.setState({
      displayStepVerification: true
    });
  }
  handleBtnAddMobileNumberClick() {
    this.setState({
      mobileNumberSetupModalDisplay: true,
      displayMobileNumberOTPVerify: false
    });
  }

  getUserProfile() {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");

      var options = {
        method: "GET",
        url: Config.IDCS_SERVICE_ENDPOINT + "/admin/v1/Me",
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + localStorage.getItem("token")
        }
      };

      request(options, function(error, response, body) {
        if (error) {
          if (response && error.response.status === 401) {
            localStorage.clear();
            window.location.replace(Config.LOGIN_URL);
          } else {
            reject(error);
          }
        }
        if (response.statusCode === 200 && body !== undefined) {
          const data = JSON.parse(body);
          const emails = data.emails;
          const name = data.name;
          const pmfa =
            data["urn:ietf:params:scim:schemas:oracle:idcs:extension:mfa:User"];

          if (emails !== undefined) {
            const primaryMail = emails.find(function(email) {
              return email.primary === true;
            });
            currentComponent.setState({ primaryMail: primaryMail });
          }
          if (pmfa && pmfa.preferredDevice) {
            currentComponent.setState({
              preferredMFADeviceId: pmfa.preferredDevice.value,
              temporaryPreferredMFADeviceId: pmfa.preferredDevice.value
            });
          }
          currentComponent.setState({ name: name });
          currentComponent.setState({ userName: data.userName });
          currentComponent.setState({ title: data.title });
          currentComponent.setState({ userId: data.id });

          resolve(data);
        }
      });
    });
  }

  async componentDidMount() {
    this.events = [
      "load",
      "mousemove",
      "mousedown",
      "click",
      "scroll",
      "keypress"
    ];
    for (var i in this.events) {
      window.addEventListener(this.events[i], this.resetTimeout);
    }
    this.setTimeout();

    await this.getUserProfile();
    await this.getStepVerificationDiviceList();
  }
  async handleBtnAddQRCodeSetupClick() {
    await this.getQRCodeImage();
    this.setState({
      mobileAppSetupModalDisplay: true
    });
  }

  async getQRCodeImage() {
    let currentComponent = this;
    return new Promise(function(resolve, reject) {
      var request = require("request");

      var options = {
        method: "POST",
        url:
          Config.IDCS_SERVICE_ENDPOINT +
          "/admin/v1/MyAuthenticationFactorEnroller",
        headers: {
          Authorization: "Bearer " + localStorage.getItem("token"),
          "Content-Type": "application/json"
        },
        body: {
          schemas: [
            "urn:ietf:params:scim:schemas:oracle:idcs:AuthenticationFactorEnroller"
          ],
          user: { value: currentComponent.state.userId },
          authnFactors: ["PUSH"]
        },
        json: true
      };

      request(options, async function(error, response, body) {
        if (error) {
          if (error.response.status === 401) {
            localStorage.clear();
            window.location.replace(Config.LOGIN_URL);
          } else {
            reject(error);
          }
        }
        if (response.statusCode === 201 && body !== undefined) {
          if (body) {
            let qrcode = new Buffer(body.qrCodeImgContent, "base64");
            let qrcontent = qrcode.toString();
            let device = {
              id: body.deviceId,
              displayName: body.displayName,
              qrCodeImgContent: body.qrCodeImgContent,
              qrCodeContent: body.qrCodeContent,
              qrCodeImgType: qrcontent,
              authnFactors: body.authnFactors
            };
            currentComponent.setState({
              otpMobileQRCode: device
            });
          }
          resolve(body);
        }
      });
    });
  }

  toggleEmailSetupModal() {
    this.setState({
      inputEmailPasscode: "",
      emailSetupModalDisplay: !this.state.emailSetupModalDisplay,
      emailSetupValidationMessage: ""
    });
  }
  toggleRemoveOTPDeviceModal() {
    this.setState({
      displayRemoveOTPDeviceModal: !this.state.displayRemoveOTPDeviceModal,
      otpRemoveHeader: "",
      otpRemoveBody: "",
      otpRemoveId: ""
    });
  }
  toggleRemoveAllOTPDeviceModal() {
    this.setState({
      displayRemoveAllOtpModal: !this.state.displayRemoveAllOtpModal,
      displayStepVerification: true
    });
  }
  toggleChangeDefaultMFAModal() {
    this.setState({
      displayChangeDefaultMFAModal: !this.state.displayChangeDefaultMFAModal,
      preferredMFADeviceId: this.state.temporaryPreferredMFADeviceId,
      disableBtnConfirmPreferredMFA: true
    });
  }
  toggleMobileAppSetupModal() {
    this.setState({
      mobileAppSetupModalDisplay: !this.state.mobileAppSetupModalDisplay
    });
    this.getStepVerificationDiviceList();
  }
  toggleMobileNumberSetupModal() {
    this.setState({
      mobileNumberSMSSetupMessage: "",
      mobileNumberSetupModalDisplay: !this.state.mobileNumberSetupModalDisplay
    });
  }

  render() {
    if (this.state.loading) {
      return (
        <div>
          <div className="sweet-loading spinner">
            <HashLoader
              sizeUnit={"px"}
              size={50}
              color={"#123abc"}
              loading={this.state.loading}
            />
          </div>
        </div>
      );
    } else {
      return (
        <div>
          <Row>
            <Col xl="10">
              <Card className="mb-0">
                <CardHeader>
                  <strong>Multi Factor Verification</strong>
                </CardHeader>
                <CardBody>
                  <Row>
                    <div>
                      <span>
                        2-Step Verification adds an additional layer of security
                        to your account by using a second device or security
                        questions to verify your identity. Once set up, other
                        users cannot access your account even if they guess your
                        password.
                      </span>
                      {this.state.displayStepVerification ? (
                        <span>
                          You can manage your 2-Step Verification methods from
                          this page.
                        </span>
                      ) : (
                        ""
                      )}
                    </div>
                  </Row>
                  <Row>
                    {!this.state.displayStepVerification ? (
                      <div style={{ marginTop: "20px" }}>
                        <Col xs="10" md="10" sm="1" className="mb-10">
                          2-Step Verification :
                          {this.state.userHasMFASetup ? (
                            <strong className="color-success">
                              {" "}
                              Enabled &nbsp;&nbsp;
                            </strong>
                          ) : (
                            <strong className="color-error">
                              {" "}
                              Disabled &nbsp;&nbsp;
                            </strong>
                          )}
                          <Button
                            color="primary"
                            name="btnDisplayStepVerification"
                            onClick={this.displayStepVerificationContent}
                          >
                            {this.state.userHasMFASetup ? "Manage" : "Setup"}
                          </Button>
                        </Col>
                      </div>
                    ) : (
                      ""
                    )}
                  </Row>
                  {this.state.displayStepVerification ? (
                    <div style={{ marginTop: "20px" }}>
                      <div style={{ paddingBottom: "15px" }}>
                        {" "}
                        <AppSwitch
                          className="mx-1"
                          color="primary"
                          size="lg"
                          checked={
                            this.state.displayStepVerification ? true : false
                          }
                          onClick={this.stepVerificationSwitch}
                        />
                        <strong className="color-success">
                          {" "}
                          &nbsp;&nbsp; Enabled
                        </strong>
                        &nbsp;&nbsp;{" "}
                        {this.state.userHasMFASetup ? (
                          <Button
                            color="primary"
                            type="button"
                            name="changeDefault"
                            id="changeDefault"
                            onClick={this.changeDefaultMFAType}
                            style={{ marginBottom: "20px" }}
                          >
                            <span>Change Default</span>
                          </Button>
                        ) : (
                          ""
                        )}
                        <Modal
                          isOpen={this.state.displayChangeDefaultMFAModal}
                          toggle={this.toggleChangeDefaultMFAModal}
                          className={"modal-primary " + this.props.className}
                        >
                          <ModalHeader
                            toggle={this.displayChangeDefaultMFAModal}
                          >
                            Change default MFA
                          </ModalHeader>
                          <ModalBody>
                            <p>
                              Select your default 2-Step Verification method.
                            </p>
                            <Table>
                              <thead>
                                <tr>
                                  <th>Name</th>
                                  <th>Method</th>
                                  <th />
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.otpEnrolledEmail.displayName !==
                                "" ? (
                                  <tr>
                                    <td>
                                      {this.state.otpEnrolledEmail.displayName}
                                    </td>
                                    <td>One-Time Passcode</td>
                                    <td>
                                      <Input
                                        className="form-check-input"
                                        type="radio"
                                        id={this.state.otpEnrolledEmail.id}
                                        checked={
                                          this.state.preferredMFADeviceId ===
                                          this.state.otpEnrolledEmail.id
                                            ? true
                                            : false
                                        }
                                        name="inputDefaultMFASelection"
                                        value={this.state.otpEnrolledEmail.id}
                                        onChange={
                                          this.handleInputDefaultMFASelection
                                        }
                                      />
                                    </td>
                                  </tr>
                                ) : (
                                  ""
                                )}
                                {this.state.listEnrolledMobileNumbers.map(
                                  mobile => (
                                    <tr key={mobile.id}>
                                      <td>{mobile.phoneNumber}</td>
                                      <td>Text Message</td>
                                      <td>
                                        <Input
                                          className="form-check-input"
                                          type="radio"
                                          id={mobile.id}
                                          name="inputDefaultMFASelection"
                                          value={mobile.id}
                                          checked={
                                            this.state.preferredMFADeviceId ===
                                            mobile.id
                                              ? true
                                              : false
                                          }
                                          onChange={
                                            this.handleInputDefaultMFASelection
                                          }
                                        />
                                      </td>
                                    </tr>
                                  )
                                )}
                                {this.state.listEnrolledQRDevices.map(
                                  device => (
                                    <tr key={device.id}>
                                      <td>{device.displayName}</td>
                                      <td>OTP</td>
                                      <td>
                                        <Input
                                          className="form-check-input"
                                          type="radio"
                                          id={device.id}
                                          name="inputDefaultMFASelection"
                                          value={device.id}
                                          checked={
                                            this.state.preferredMFADeviceId ===
                                            device.id
                                              ? true
                                              : false
                                          }
                                          onChange={
                                            this.handleInputDefaultMFASelection
                                          }
                                        />
                                      </td>
                                    </tr>
                                  )
                                )}
                              </tbody>
                            </Table>
                          </ModalBody>
                          <ModalFooter>
                            <Button
                              color="primary"
                              className="px-4"
                              type="button"
                              name="BtnChangeDefaultConfirm"
                              disabled={
                                this.state.disableBtnConfirmPreferredMFA
                              }
                              onClick={this.handleBtnChangeDefaultMFAConfirm}
                            >
                              <span>Set as Default</span>
                            </Button>
                            <Button
                              color="secondary"
                              onClick={this.toggleChangeDefaultMFAModal}
                            >
                              Cancel
                            </Button>
                          </ModalFooter>
                        </Modal>
                      </div>
                      {this.state.showErrorMessage ? (
                        <span className="color-error">
                          {this.state.errorMessage}
                        </span>
                      ) : (
                        ""
                      )}
                      <br />
                      <Accordion allowMultipleExpanded="true">
                        <AccordionItem>
                          <AccordionItemHeading>
                            <AccordionItemButton>Emails</AccordionItemButton>
                          </AccordionItemHeading>
                          <AccordionItemPanel>
                            {this.state.emailSetupProcessCompleted ? (
                              <div>
                                <Table>
                                  <thead>
                                    <tr>
                                      <th>Email Address </th>
                                      <th>Remove</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr>
                                      <td>
                                        {
                                          this.state.otpEnrolledEmail
                                            .displayName
                                        }
                                      </td>
                                      <td>
                                        <Button
                                          color="primary"
                                          className=""
                                          size="sm"
                                          type="button"
                                          id={this.state.otpEnrolledEmail.id}
                                          name="btnRemoveOTPEmail"
                                          onClick={this.btnRemoveOTPDevice}
                                        >
                                          <i className="fa fa-minus padding-right" />
                                          <span>Remove</span>
                                        </Button>
                                      </td>
                                    </tr>
                                  </tbody>
                                </Table>
                              </div>
                            ) : (
                              <div>
                                <Row>
                                  <Col md="8">
                                    Your email is not set up as a 2-Step
                                    Verification method
                                  </Col>
                                </Row>
                                <Row>
                                  <Col md="8" className="div-margin">
                                    <Button
                                      color="primary"
                                      className="px-4"
                                      type="button"
                                      name="btnEmailSetup"
                                      onClick={this.handleBtnEmailSetupClick}
                                    >
                                      <i className="fa fa-plus padding-right" />
                                      <span>Setup</span>
                                    </Button>
                                  </Col>
                                </Row>
                              </div>
                            )}
                            <Modal
                              isOpen={this.state.emailSetupModalDisplay}
                              toggle={this.toggleEmailSetupModal}
                              className={
                                "modal-primary " + this.props.className
                              }
                            >
                              <ModalHeader toggle={this.toggleEmailSetupModal}>
                                Email
                              </ModalHeader>
                              <ModalBody>
                                <div className="div-margin">
                                  {this.state.emailSetupProcessCompleted ? (
                                    <Row>
                                      <Col md="16">
                                        <span>
                                          Your primary email address has been
                                          successfully set up for 2-Step
                                          Verification.
                                        </span>
                                      </Col>
                                    </Row>
                                  ) : (
                                    <div>
                                      <Row>
                                        <Col md="16">
                                          An email that contains a passcode has
                                          been sent to{" "}
                                          <strong>
                                            {this.state.primaryMail.value}.
                                          </strong>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="10" className="div-margin">
                                          <InputGroup>
                                            <Input
                                              type="text"
                                              name="inputEmailPasscode"
                                              placeholder="Enter Passcode"
                                              value={
                                                this.state.inputEmailPasscode
                                              }
                                              onChange={
                                                this
                                                  .handleOnChangeInputEmailPasscode
                                              }
                                            />
                                            <InputGroupAddon addonType="append">
                                              <Button
                                                type="button"
                                                color="primary"
                                                name="btnVerifyEmailPasscode"
                                                onClick={
                                                  this
                                                    .handleBtnVerifyEmailPasscodeClick
                                                }
                                              >
                                                Verify
                                              </Button>
                                            </InputGroupAddon>
                                          </InputGroup>
                                        </Col>
                                      </Row>
                                      <Row>
                                        <Col md="16">
                                          <span className="color-error">
                                            {
                                              this.state
                                                .emailSetupValidationMessage
                                            }
                                          </span>
                                        </Col>
                                      </Row>
                                    </div>
                                  )}
                                </div>
                              </ModalBody>
                              <ModalFooter>
                                <Button
                                  color="secondary"
                                  onClick={this.toggleEmailSetupModal}
                                >
                                  Close
                                </Button>
                              </ModalFooter>
                            </Modal>
                          </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem>
                          <AccordionItemHeading>
                            <AccordionItemButton>
                              Mobile Number
                            </AccordionItemButton>
                          </AccordionItemHeading>
                          <AccordionItemPanel>
                            <Row>
                              {!this.state.hideEnroleMobileSMSOTPMessage ? (
                                <Col md="8">
                                  You don't have any configured mobile numbers
                                </Col>
                              ) : (
                                ""
                              )}
                              <Col md="4">
                                <Button
                                  color="primary"
                                  className="px-4"
                                  type="button"
                                  name="btnAddMobileNumber"
                                  onClick={this.handleBtnAddMobileNumberClick}
                                >
                                  <i className="fa fa-plus padding-right" />
                                  <span>Add</span>
                                </Button>
                              </Col>
                            </Row>
                            <br />
                            <Table responsive striped>
                              <thead>
                                <tr>
                                  <th scope="col">Name</th>
                                  <th scope="col">Number</th>
                                  <th scope="col">Remove</th>
                                </tr>
                              </thead>
                              <tbody>
                                {this.state.listEnrolledMobileNumbers.map(
                                  mobile => (
                                    <tr key={mobile.id}>
                                      <td>{mobile.displayName}</td>
                                      <td>{mobile.phoneNumber}</td>
                                      <td>
                                        <Button
                                          color="primary"
                                          className="px-4 remove-label"
                                          size="sm"
                                          type="button"
                                          name={
                                            mobile.authenticationFactors[0].type
                                          }
                                          id={mobile.id}
                                          onClick={this.btnRemoveOTPDevice}
                                        >
                                          <i className="fa fa-minus padding-right" />
                                          <span>Remove</span>
                                        </Button>
                                      </td>
                                    </tr>
                                  )
                                )}
                              </tbody>
                            </Table>
                            <Modal
                              isOpen={this.state.mobileNumberSetupModalDisplay}
                              toggle={this.toggleMobileNumberSetupModal}
                              className={
                                "modal-primary " + this.props.className
                              }
                            >
                              <ModalHeader
                                toggle={this.toggleMobileNumberSetupModal}
                              >
                                Mobile Number
                              </ModalHeader>
                              <ModalBody>
                                <div className="div-margin">
                                  <Row>
                                    <Col md="16">
                                      <span>
                                        We will text (SMS) you a passcode to
                                        verify that you are the owner of this
                                        phone.
                                      </span>
                                    </Col>
                                  </Row>

                                  <div style={{ paddingTop: "20px" }}>
                                    <FormGroup row>
                                      <Col md="4">Name</Col>
                                      <Col md="6" className="text-align-left">
                                        <Input
                                          type="text"
                                          disabled={
                                            this.state
                                              .disableinputAddMobileModalField
                                          }
                                          name="inputAddMobileModalName"
                                          onChange={
                                            this
                                              .handleAddMobileNumberModalChange
                                          }
                                        />
                                      </Col>
                                    </FormGroup>
                                    <FormGroup row>
                                      <Col md="4">Mobile Number</Col>

                                      <Col md="6" className="text-align-left">
                                        <Input
                                          disabled={
                                            this.state
                                              .disableinputAddMobileModalField
                                          }
                                          type="number"
                                          name="inputAddMobileModalNumber"
                                          onChange={
                                            this
                                              .handleAddMobileNumberModalChange
                                          }
                                        />
                                      </Col>
                                    </FormGroup>
                                    {this.state.displayMobileNumberOTPVerify ? (
                                      <div>
                                        <FormGroup row>
                                          <Col md="10">
                                            A passcode has been sent to{" "}
                                            {
                                              this.state
                                                .inputAddMobileModalNumber
                                            }
                                          </Col>
                                        </FormGroup>
                                        <InputGroup style={{ width: "60%" }}>
                                          <Input
                                            type="text"
                                            name="inputMobileSMSOtp"
                                            placeholder="SMS OTP"
                                            value={this.state.inputMobileSMSOtp}
                                            onChange={
                                              this
                                                .handleAddMobileNumberModalChange
                                            }
                                          />
                                          <InputGroupAddon addonType="append">
                                            <Button
                                              type="button"
                                              color="primary"
                                              name="btnVerifyMobileSMSOtp"
                                              onClick={
                                                this
                                                  .handleBtnVerifyMobileSMSOtpClick
                                              }
                                            >
                                              Verify
                                            </Button>
                                          </InputGroupAddon>
                                        </InputGroup>{" "}
                                      </div>
                                    ) : (
                                      ""
                                    )}
                                  </div>
                                  <Row>
                                    <Col md="16">
                                      <span className="color-error">
                                        {this.state.mobileNumberSMSSetupMessage}
                                      </span>
                                    </Col>
                                  </Row>
                                </div>
                              </ModalBody>
                              <ModalFooter>
                                {!this.state.displayMobileNumberOTPVerify ? (
                                  <Button
                                    color="primary"
                                    className="px-4"
                                    type="button"
                                    name="btnAddMobileNumberSendOTP"
                                    onClick={
                                      this.handleBtnAddMobileNumberSendOTPClick
                                    }
                                  >
                                    <span>Send OTP</span>
                                  </Button>
                                ) : (
                                  ""
                                )}{" "}
                                <Button
                                  color="secondary"
                                  onClick={this.toggleMobileNumberSetupModal}
                                >
                                  Cancel
                                </Button>
                              </ModalFooter>
                            </Modal>
                          </AccordionItemPanel>
                        </AccordionItem>
                        <AccordionItem>
                          <AccordionItemHeading>
                            <AccordionItemButton>
                              Mobile App
                            </AccordionItemButton>
                          </AccordionItemHeading>
                          <AccordionItemPanel>
                            <div>
                              {!this.state.hideEnroleMobileAppMessage ? (
                                <Row>
                                  <Col md="8">
                                    You don't have any configured mobile apps
                                  </Col>
                                </Row>
                              ) : (
                                ""
                              )}
                              <Row>
                                <Col md="8" className="div-margin">
                                  <Button
                                    color="primary"
                                    className="px-4"
                                    type="button"
                                    name="btnEmailSetup"
                                    onClick={this.handleBtnAddQRCodeSetupClick}
                                  >
                                    <i className="fa fa-plus padding-right" />
                                    <span>Add</span>
                                  </Button>
                                </Col>
                              </Row>
                              <Table responsive striped>
                                <thead>
                                  <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Method</th>
                                    <th scope="col">Remove</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  {this.state.listEnrolledQRDevices.map(
                                    device => (
                                      <tr key={device.id}>
                                        <td>{device.displayName}</td>
                                        <td>One-Time Passcode</td>
                                        <td>
                                          <Button
                                            color="primary"
                                            className="px-4 remove-label"
                                            size="sm"
                                            type="button"
                                            name={
                                              device.authenticationFactors[0]
                                                .type
                                            }
                                            id={device.id}
                                            onClick={this.btnRemoveOTPDevice}
                                          >
                                            <i className="fa fa-minus padding-right" />
                                            <span>Remove</span>
                                          </Button>
                                        </td>
                                      </tr>
                                    )
                                  )}
                                </tbody>
                              </Table>
                            </div>
                            <Modal
                              isOpen={this.state.mobileAppSetupModalDisplay}
                              toggle={this.toggleMobileAppSetupModal}
                              className={
                                "modal-primary " + this.props.className
                              }
                            >
                              <ModalHeader
                                toggle={this.toggleMobileAppSetupModal}
                              >
                                Add Mobile App
                              </ModalHeader>
                              <ModalBody>
                                <div className="div-margin">
                                  <div>
                                    <Row>
                                      <Col md="16">
                                        <strong>
                                          Download and Configure the Mobile App
                                        </strong>
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col md="16">
                                        1. Download the Oracle Mobile
                                        Authenticator App from the app store.
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col md="16">
                                        2. Open the App, tap Add Account, and
                                        then scan the QR code below.
                                      </Col>
                                    </Row>
                                    <Row>
                                      <Col md="16">
                                        <div className="qrcode-img">
                                          <img
                                            alt="qrcode"
                                            className="qr-img"
                                            id="enrollimage"
                                            src={
                                              "data:image/png;base64," +
                                              this.state.otpMobileQRCode
                                                .qrCodeImgType
                                            }
                                          />
                                        </div>
                                      </Col>
                                    </Row>
                                  </div>
                                </div>
                              </ModalBody>
                              <ModalFooter>
                                <Button
                                  color="secondary"
                                  onClick={this.toggleMobileAppSetupModal}
                                >
                                  Close
                                </Button>
                              </ModalFooter>
                            </Modal>
                            <Modal
                              isOpen={this.state.displayRemoveOTPDeviceModal}
                              toggle={this.toggleRemoveOTPDeviceModal}
                              className={
                                "modal-primary " + this.props.className
                              }
                            >
                              <ModalHeader
                                toggle={this.toggleRemoveOTPDeviceModal}
                              >
                                {this.state.otpRemoveHeader}
                              </ModalHeader>
                              <ModalBody>
                                <p>{this.state.otpRemoveBody}</p>
                              </ModalBody>
                              <ModalFooter>
                                <Button
                                  color="primary"
                                  className="px-4"
                                  type="button"
                                  name="BtnRemoveOTPConfirm"
                                  onClick={this.handleBtnRemoveOTPConfirm}
                                >
                                  <span>Confirm</span>
                                </Button>
                                <Button
                                  color="secondary"
                                  onClick={this.toggleRemoveOTPDeviceModal}
                                >
                                  Close
                                </Button>
                              </ModalFooter>
                            </Modal>
                            <Modal
                              isOpen={this.state.displayRemoveAllOtpModal}
                              toggle={this.toggleRemoveAllOTPDeviceModal}
                              className={
                                "modal-primary " + this.props.className
                              }
                            >
                              <ModalHeader
                                toggle={this.displayRemoveAllOtpModal}
                              >
                                Disable 2-Step Verification
                              </ModalHeader>
                              <ModalBody>
                                <p>
                                  Are you sure you want to disable 2-Step
                                  verification?
                                </p>
                              </ModalBody>
                              <ModalFooter>
                                <Button
                                  color="primary"
                                  className="px-4"
                                  type="button"
                                  name="BtnRemoveAllOTPConfirm"
                                  onClick={this.handleBtnRemoveAllOTP}
                                >
                                  <span>Confirm</span>
                                </Button>
                                <Button
                                  color="secondary"
                                  onClick={this.toggleRemoveAllOTPDeviceModal}
                                >
                                  Cancel
                                </Button>
                              </ModalFooter>
                            </Modal>
                          </AccordionItemPanel>
                        </AccordionItem>
                      </Accordion>
                    </div>
                  ) : (
                    ""
                  )}
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      );
    }
  }
  clearTimeoutFunc = () => {
    if (this.logoutTimeout) clearTimeout(this.logoutTimeout);
  };

  setTimeout = () => {
    this.logoutTimeout = setTimeout(this.logout, this.state.signoutTime);
  };

  resetTimeout = () => {
    this.clearTimeoutFunc();
    this.setTimeout();
  };

  logout = () => {
    console.log("Sending a logout request...");
    this.destroy();
  };

  destroy = () => {
    localStorage.clear();
    window.location.replace(Config.LOGOUT_URL);
  };
}

export default MultiFactor;
