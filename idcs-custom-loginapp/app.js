/*
 * File containing all the routes for the Sample Application.
 * @Copyright Oracle
 */

//Import libraries
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var exphbs = require("express-handlebars");
var expressValidator = require("express-validator");
var session = require("express-session");
var passport = require("passport");
var OIDCStrategy = require("passport-idcs").OIDCStrategy;
var IdcsAuthenticationManager = require("passport-idcs")
  .IdcsAuthenticationManager;
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var cors = require("cors");
var request = require("request");

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "1";
//Loading the configurations
var auth = require("./auth.js");
var config = require("./config.js");

console.log("\nUsing ClientId=" + auth.oracle.ClientId);

//Init app for express framework.
var app = express();

var logger = require("morgan");
// By including this up here we go get an AT as soon as we're started
// and if there's an exception (e.g. the hostname, client ID or secret are bad)
// then it will throw an exception and we'll shutdown gracefully.
var oauth = require("./helpers/oauth");

var indexRouter = require("./routes/index");
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "DELETE, PUT, GET, POST");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});
app.use("/", indexRouter);

app.post("/changePassword", function(req, res) {
  var xhr = new XMLHttpRequest();
  xhr.open(
    "POST",
    "https://t-identity.sfgov.org/securityprofile/restapi/ccsfpwdsvc/userPwdChanger"
  );
  xhr.setRequestHeader("Content-Type", "application/json");
  var reqdata = JSON.stringify({
    userId: req.body.userid,
    password: req.body.password,
    newPassword: req.body.newPassword
  });
  console.log(req.body.userid);

  xhr.addEventListener("readystatechange", function() {
    console.log(xhr.status);
    if (xhr.readyState === 4) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
      );

      if (xhr.status === 200) {
        // self.app.logMsg ('Authenticate response: ' + self.app.mask(this.responseText));

        //    console.log('============>'+this.responseText);
        //    var mydata = JSON.parse(this.responseText);
        //    console.log(mydata.totalResults+'mydata');

        //        res.header("Access-Control-Allow-Origin", "*");
        //        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.send("SUCCESS");
        res.end();
        console.log("===============> in the listener");
      } else {
        res.send("ERROR");
        //res.send("Detail:Unable to reset password please try again");
        res.end();
      }
    }
  });
  xhr.send(reqdata);
});

app.get("/userquery", function(req, res) {
  getAT()
    .then(function(accessToken) {
      console.log("Access token:\n" + accessToken);
      console.log("Acquired initial Access Token successfully.\n");
      console.log("Request data is: ");
      console.log(req.url + "\n");
      var xhr = new XMLHttpRequest();
      var uid = req.url.split("=");
      uid = uid[1];
      console.log("uid" + uid);
      //get user data from idcs
      xhr.open(
        "GET",
        process.env.IDCS_URL +
          "/admin/v1/Users?attributes=userName&filter=urn:ietf:params:scim:schemas:extension:enterprise:2.0:User:employeeNumber sw " +
          uid
      );

      xhr.setRequestHeader("Content-Type", "application/scim+json");
      xhr.setRequestHeader("Authorization", "Bearer " + accessToken);
      xhr.send();
      //console.log(document.getElementById("userid").value+'=userid');

      var newid = xhr.addEventListener("readystatechange", function() {
        if (this.readyState === 4) {
          // self.app.logMsg ('Authenticate response: ' + self.app.mask(this.responseText));

          console.log(this.responseText);
          var mydata = JSON.parse(this.responseText);
          console.log(mydata.totalResults + "mydata");
          if (mydata.totalResults > 0) {
            var arr = mydata.Resources;
            for (var i = 0; i < arr.length; i++) {
              console.log(arr[i].userName);
              newid = arr[i].userName;
              mydata = { newid: newid };
            }
          }
          res.header("Access-Control-Allow-Origin", "*");
          res.header(
            "Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type, Accept"
          );
          res.send(mydata);
          res.end();
          console.log("response from server " + mydata);
        }
      });

      //res.write('Hello World!');
      //res.end();
    })
    .catch(err => console.error(err));
});

app.post("/verifypwd", function(req, res) {
  var xhr = new XMLHttpRequest();
  xhr.open(
    "POST",
    "https://t-bifrost.sfgov.org/securityprofile/restapi/ccsfpwdsvc/checkuserpwd"
  );
  //xhr.open("POST","https://rci-iam-tst04.sfgov.org:14115/securityprofile/restapi/ccsfpwdsvc/checkuserpwd");
  xhr.setRequestHeader("Content-Type", "application/json");
  var reqdata = JSON.stringify({
    userId: req.body.userid
  });
  console.log(reqdata);

  xhr.addEventListener("readystatechange", function() {
    console.log(xhr.status);
    if (xhr.readyState === 4) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
      );

      if (xhr.status === 200) {
        console.log("============>" + this.responseText);

        res.header("Access-Control-Allow-Origin", "*");
        res.header(
          "Access-Control-Allow-Headers",
          "Origin, X-Requested-With, Content-Type, Accept"
        );
        res.send(this.responseText);
        res.end();
        console.log("===============> in the listener");
      } else {
        res.send("ERROR");
        res.end();
      }
    }
  });
  xhr.send(reqdata);
});

app.post("/sendOTP", async function(req, res) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );

  //console.log(data);
  try {
    let otp = await requestSendOTP(req.body.UserLogin);
    res.status(200).send(otp);
  } catch (e) {
    console.log(e);
    res.status(400).send("Failed");
  }
});

async function requestSendOTP(id) {
  return new Promise(function(resolve, reject) {
    var options = {
      method: "POST",
      url:
        "https://t-identity.sfgov.org/securityprofile/restapi/ccsfpwdsvc/sendOTP",
      headers: {
        "Content-Type": "application/json"
      },
      body: {
        ForgotPassword: {
          UserLogin: id
        }
      },
      json: true
    };
    request(options, function(error, response, body) {
      if (error) {
        console.log(error);
        reject(error);
      }
      if (response) {
        console.log(body);
        resolve(body.ForgotPassword);
      }
    });
  });
}

app.post("/verifyOTP", async function(req, res) {
  console.log("=================In the app verifyOTP");
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );

  //console.log(data);
  try {
    let otp = await verifyPassordRecoveryOTP(
      req.body.UserLogin,
      req.body.UserGivenOTP
    );
    console.log("========>" + otp);
    res.status(200).send(otp);
  } catch (e) {
    console.log(e);
    res.status(400).send("Failed");
  }
});

async function verifyPassordRecoveryOTP(userName, otpValue) {
  if (userName === null || otpValue === null) {
    return "Invalid fields.";
  }
  return new Promise(function(resolve, reject) {
    var request = require("request");

    var options = {
      method: "POST",
      url:
        "https://t-identity.sfgov.org/securityprofile/restapi/ccsfpwdsvc/verifyOTP",
      headers: {
        "Content-Type": "application/json"
      },
      body: {
        ForgotPassword: {
          UserLogin: userName,
          UserGivenOTP: otpValue
        }
      },
      json: true
    };

    request(options, function(error, response, body) {
      if (error) {
        logger.error("Request to verify OTP access token failed.");
        reject("FAILED");
      }
      if (response) {
        console.log("================+>" + body.ForgotPassword);
        resolve(body.ForgotPassword);
      }
    });
  });
}

app.post("/resetPwd", async function(req, res) {
  console.log("=================In the app resetPwd");
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );

  //console.log(data);
  try {
    let otp = await otpResetPassord(req.body.UserLogin, req.body.NewPassword);
    console.log("========>" + otp);
    res.status(200).send(otp);
  } catch (e) {
    console.log(e);
    res.status(400).send("Failed");
  }
});
async function otpResetPassord(userName, password) {
  return new Promise(function(resolve, reject) {
    var request = require("request");

    var options = {
      method: "POST",
      url:
        "https://t-identity.sfgov.org/securityprofile/restapi/ccsfpwdsvc/resetPwd",
      headers: {
        "Content-Type": "application/json"
      },
      body: {
        ForgotPassword: {
          UserLogin: userName,
          NewPassword: password
        }
      },
      json: true
    };

    request(options, function(error, response, body) {
      if (error) {
        logger.error("Request to verify OTP access token failed.");
        reject("FAILED");
      }
      if (response && 200 === response.statusCode && body) {
        resolve(body);
      }
      if (response && 400 === response.statusCode && body) {
        reject(body);
      }
    });
  });
}

function getAT() {
  return new Promise(function(resolve, reject) {
    request(
      {
        method: "POST",
        uri: process.env.IDCS_URL + "/oauth2/v1/token",
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          Authorization:
            "Basic " +
            Buffer.from(
              "5a8144bd097748ffb020b460f274db77" +
                ":" +
                "24e0f673-b9f9-42fa-9a0d-1afd241fb046"
            ).toString("base64"),
          Accept: "application/json"
        },

        body:
          "grant_type=client_credentials&scope=" +
          encodeURIComponent(neededScopes.join(" "))
      },
      function(error, response, body) {
        if (error) console.log("error: " + error);
        if (response && response.statusCode) {
          //console.log('statusCode: ' + response.statusCode);
        }
        //  console.log('body: ' + body);

        if (response && 200 == response.statusCode) {
          var bodydata = JSON.parse(body);
          let token = bodydata.access_token;
          var decoded = jwt.decode(token);
        }
        if (response && 200 == response.statusCode) {
          var bodydata = JSON.parse(body);
          let token = bodydata.access_token;
          var decoded = jwt.decode(token);
          resolve(token);
        }
      }
    );
  });
}

let neededScopes = ["urn:opc:idm:__myscopes__"];

// those scopes are currently included in these IDCS app roles:
var necessaryAppRoles = ["Identity Domain Administrator"];

function getSigningKey(accessToken, url) {
  return new Promise(function(resolve, reject) {
    request(
      {
        method: "GET",
        uri:
          "https://idcs-28d2cb948ab8484aaeb29f302ed52c7d.identity.oraclecloud.com" +
          "admin/v1" +
          url,
        headers: {
          "Content-type": "application/x-www-form-urlencoded",
          Authorization: "Bearer " + accessToken,
          Accept: "application/json"
        }
      },
      function(error, response, body) {
        if (error) console.log("error: " + error);
        if (response && response.statusCode) {
          console.log("statusCode: " + response.statusCode);
        }
        console.log("body: " + body);

        if (response && 200 == response.statusCode) {
          var bodydata = JSON.parse(body);
          console.log(JSON.stringify(bodydata, null, 2));
          // we need the first (and probably only) cert from there
          return;
        }

        // if we get down to here there was a problem.
        // for now we just throw a generic error.
        // since this function is only called during startup throwing here
        // will crash out of the startup and shut the server down.
        // I *think* that's what we want.
        throw "Failed to acquire certificate from JWKS URI!";
      }
    );
  });
}

console.log("Starting and Listening");

// View Engine setting
app.set("views", path.join(__dirname, "views"));
app.engine(
  "handlebars",
  exphbs({
    defaultLayout: "publicLayout",
    helpers: {
      toString: function(object) {
        return JSON.stringify(object, null, 2);
      },
      ifequals: function(obj1, obj2, options) {
        if (obj1 == obj2) {
          return options.fn(this);
        } else {
          return options.inverse(this);
        }
      }
    }
  })
);

app.set("view engine", "handlebars");

//BodyParser middleware setting
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

//Public folder for static content.
app.use(express.static(path.join(__dirname, "public")));

//Express Session setting
app.use(
  session({
    secret: "sample_application",
    saveUninitialized: true,
    resave: true
  })
);

// serialize and deserialize
passport.serializeUser(function(user, done) {
  done(null, user);
});
passport.deserializeUser(function(obj, done) {
  done(null, obj);
});

passport.use(
  new OIDCStrategy(auth.oracle, (idToken, tenant, user, done) => {
    done(null, user);
  })
);

//Express Validator setting
app.use(
  expressValidator({
    errorFormatter: function(param, msg, value) {
      var namespace = param.split("."),
        root = namespace.shift(),
        formParam = root;

      while (namespace.length) {
        formParam += "[" + namespace.shift() + "]";
      }
      return {
        param: formParam,
        msg: msg,
        value: value
      };
    }
  })
);

//Global variables
app.use(function(req, res, next) {
  res.locals.user = req.user || null;
  next();
});

//Passport initialization
app.use(passport.initialize());
app.use(passport.session());

//-------------
//Routes section
//Public acess
app.get("/", function(req, res) {
  res.render("index", { title: "Index" });
});
app.get("/login", function(req, res) {
  res.render("login", { title: "Login" });
});
app.get("/about", function(req, res) {
  res.render("about", { title: "About" });
});

//Route for /logout.
//The Node.js express framework defines a mechanism to log the user out of the web application.
//The handler of the /logout route uses the res.clearCookie() express function to clear all of the cookies that have been set,
//and then redirects the browser to Oracle Identity Cloud Service's log out URL sending parameters to redirect the user browser
//back to the sample application after logging  out from Oracle Identity Cloud Service.
app.get("/logout", function(req, res) {
  console.log(
    "\n---Resource: /logout -- Logging out ----------------------------------"
  );
  var id_token = req.session.id_token;
  var logouturl =
    auth.oracle.AudienceServiceUrl +
    auth.oracle.logoutSufix +
    "?post_logout_redirect_uri=http%3A//localhost%3A3000&id_token_hint=" +
    id_token;
  console.log("\nlogouturl: " + logouturl);
  req.session.destroy(function(err) {
    if (err) {
      console.log(err);
    } else {
      console.log("logging out...");
      req.logout();
      res.clearCookie();
      res.redirect(logouturl);
    }
  });
});

//Route for /oauth/oracle
//The handler of the /auth/oracle route uses the IdcsAuthenticationManager.getAuthorizationCodeUrl() SDK's function to generate the authorization URL.
app.get("/auth/oracle", function(req, res) {
  console.log(
    "\n---Resource: /auth/oracle -- Logging in ----------------------------------"
  );
  //Authentication Manager loaded with the configurations.
  am = new IdcsAuthenticationManager(auth.oracle);
  //Using Authentication Manager to generate the Authorization Code URL, passing the
  //application's callback URL as parameter, along with code value and code parameter.
  //The IdcsAuthenticationManager.getAuthorizationCodeUrl() SDK's function uses promise to redirect the request upon successful
  //generation of the authorization code URL, or to render an error instead
  am.getAuthorizationCodeUrl(
    auth.oracle.redirectURL,
    auth.oracle.scope,
    "1234",
    "code"
  )
    .then(function(authZurl) {
      console.log("\nauthZurl=" + authZurl);
      //Redirecting the browser to the Oracle Identity Cloud Service Authorization URL.
      res.redirect(authZurl);
    })
    .catch(function(err) {
      res.end(err);
    });
});

//Route for /callback
//The sample application handles the /callback route, and uses the authorization code, sent as a query parameter,
//to request an access token. The access token is stored as a cookie, and then sent to the browser for future use.
app.get("/callback", function(req, res) {
  console.log(
    "\n---Resource: /callback -- Exchanging authzcode for a token ---------------------"
  );
  //Authentication Manager loaded with the configurations.
  var am = new IdcsAuthenticationManager(auth.oracle);
  //Getting the authorization code from the "code" parameter
  var authZcode = req.query.code;
  console.log("\nauthZcode=" + authZcode);
  //Using the Authentication Manager to exchange the Authorization Code to an Access Token.
  //The IdcsAuthenticationManager.authorizationCode() SDK's function uses promise (then/catch statement) to set
  //the access token as a cookie, and to redirect the browser to the /auth.html page.
  am.authorizationCode(authZcode)
    .then(function(result) {
      //Getting the Access Token Value.
      console.log("result.access_token = " + result.access_token);
      console.log("result.id_token = " + result.id_token);
      req.session.access_token = result.access_token;
      req.session.id_token = result.id_token;
      res.cookie(config.IDCS_COOKIE_NAME, result.access_token);
      //  res.header('idcs_user_assertion', result.access_token);
      res.redirect("/auth");
      // res.redirect('/home')
    })
    .catch(function(err) {
      res.end(err);
    });
});

//Uses passport to create a User Session in Node.js.
//Passport sets a user attribute in the request as a json object.
//The /auth route's handler calls the passport.authenticate() function, with Oracle Identity Cloud Service's strategy
//name as a parameter to set up the user session, and then redirects the browser to the /home URL.
app.get("/auth", passport.authenticate(config.IDCS_STRATEGY_NAME, {}), function(
  req,
  res
) {
  console.log(
    "\n---Resource: /auth -- passport.authenticate ---------------------"
  );
  res.redirect("/home");
});

//The /home URL is a protected resource.
//The sample web application uses the ensureAuthenticated function to handle these protected resources.
//Protected route. Uses ensureAuthenticated function.
app.get("/home", ensureAuthenticated, function(req, res) {
  console.log("\n---Resource: /home -- Rendering home ---------------------");
  res.render("home", {
    layout: "privateLayout",
    title: "Home",
    user: req.user
  });
});
//Protected route. Uses ensureAuthenticated function. Diplays user information in the screen.
//The /myProfile route's handler calls the IdcsAuthenticationManager.validateIdToken() SDK's function get a json object from the id token,
//and sends it to the myProfile.handlebars file to be rendered in the browser.
app.get("/myProfile", ensureAuthenticated, function(req, res) {
  console.log(
    "\n---Resource: /myProfile -- Listing user information ---------------------"
  );
  var am = new IdcsAuthenticationManager(auth.oracle);
  //Validating id token to acquire information such as UserID, DisplayName, list of groups and AppRoles assigned to the user.
  am.validateIdToken(req.session["id_token"])
    .then(function(idToken) {
      res.render("myProfile", {
        layout: "privateLayout",
        title: "My Profile",
        user: req.user,
        userInfo: JSON.stringify(idToken, null, 2)
      });
    })
    .catch(function(err1) {
      res.end(err1);
    });
});

// Set Port
app.set("port", process.env.PORT || config.APP_DEFAULT_PORT);
app.listen(app.get("port"), function() {
  console.log("Server started on port " + app.get("port"));
});
/*
https
  .createServer(
    {
      key: fs.readFileSync("/u01/certs/key.pem"),
      cert: fs.readFileSync("/u01/certs/cert.pem"),
      passphrase: "t4sti4m33sf"
    },
    app
  )
  .listen(3021);
  */

//Function to help verify if the user is authenticated in Passwport.
function ensureAuthenticated(req, res, next) {
  console.log(
    "\n---function ensureAuthenticated() -- Validating user logged in ---------------------"
  );
  // console.log('#################################################req.user='+ JSON.stringify(req));
  console.log("req.user=" + JSON.stringify(req.user));
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect("/login");
}
module.exports = app;
