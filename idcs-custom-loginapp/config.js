var config = {
  APP_DEFAULT_PORT:'3011',
  IDCS_COOKIE_NAME:'idcs_user_assertion',
  IDCS_STRATEGY_NAME:'IDCSOIDC'
}
module.exports = config;
